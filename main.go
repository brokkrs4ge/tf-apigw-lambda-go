package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/go-bakers/gatewayx/aws/lambda"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/health", Health)
	lambda.ListenAndServe("-1", r)
}

func Health(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"status":"ok"}`))

}
