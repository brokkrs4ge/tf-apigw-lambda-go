# About #
bootstrap repo for AWS lambda  + ApiGateway Deplyoment on via terraform.

Contains a `/health` endpoint

Assumes that s3 bucket and dynamodb already created for backend and statelock.

## TODO ##
_/ terraform backend setup

# Deployment #
build binary
```sh
make build
```

run terraform scripts
```sh
make terraform-init

make terraform-plan
	
make terraform-apply


```
make functions available for localstack deployment too.

# Localstack API Gateway #
## Health ##
the `health` endpoint is reachable via `http://localhost:4566/restapis/${rest_api_id}/local/_user_request_/health`
