locals {
  region  = "us-east-1"
  profile = "localstack2"
  app_id  = "tf-apigw-lambda-go"

  backend_s3_bucket = "tfstate-s3bucket"
}
provider "aws" {
  region  = local.region
  profile = local.profile

}

data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file = "../build/bin/app"
  output_path = "build/bin/app.zip"
}


terraform {
  backend "s3" {}
}
