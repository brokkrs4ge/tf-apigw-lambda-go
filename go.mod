module pls-change-me

go 1.18

require (
	github.com/gorilla/mux v1.8.0
	gitlab.com/go-bakers/gatewayx v0.0.0-20230228162647-59b8aba70dbc
)

require (
	github.com/aws/aws-lambda-go v1.37.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
