cleanbuildfolder:
	rm -rf build/
	
build: cleanbuildfolder
	@GOOS=linux GOARCH=amd64 go build -o build/bin/app .

tflocal-init: # runs an init against localstack. assumes s3 bucket and dynamodb table already created
	@tflocal -chdir=infra init -backend-config=backend.local.conf 

tflocal-plan:
	@tflocal -chdir=infra plan

tflocal-apply:
	@tflocal -chdir=infra apply


terraform-init:
	terraform -chdir=infra init 

terraform-plan:
	terraform -chdir=infra plan
	
terraform-apply:
	terraform -chdir=infra apply
